import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Register from './views/Register.vue'
import LogIn from './views/LogIn.vue'
import Panel from './views/Panel.vue'

import firebase from 'firebase/app'
import 'firebase/auth'

Vue.use(Router);

const router =  new Router({
  routes: [
    {
      path: '*',
      redirect: '/'
    },
    //home
    {
      path: '/',
      name: 'home',
      component: Home
    },

    {
      path: '/LogIn',
      name : 'LogIn',
      component : LogIn
    },

    {
      path: '/register',
      name : 'register',
      component : Register
    },
    
    {
      path: '/panel',
      name: 'panel',
      component: Panel,
      meta: {
        needAuth:true
      }
    }
  ]
})

//GUARDS BLOCK router
router.beforeEach((to, from, next) => {
    let isUser = firebase.auth().currentUser;
    let needAuth = to.matched.some(record => record.meta.needAuth);
  
      needAuth ? (
          isUser ? next() : next({name: LogIn}) 
      ): next()
  
  });


export default router