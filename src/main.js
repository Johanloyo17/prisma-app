import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
import Element from 'element-ui';
import firebase from 'firebase'
import store from './store'
import vueSmoothScroll from 'vue2-smooth-scroll'

Vue.use(vueSmoothScroll)
Vue.use(Element, { size: 'medium', zIndex: 3000 });


Vue.config.productionTip = false;
firebase.auth().onAuthStateChanged( function (user) {  
  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app')
})



