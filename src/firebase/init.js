import firebase from 'firebase';
import firestore from 'firebase/firestore';

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyClbZ-pSFH_OE8o1JpnHs9EnLDY7_swqj8",
    authDomain: "sushimi-app.firebaseapp.com",
    databaseURL: "https://sushimi-app.firebaseio.com",
    projectId: "sushimi-app",
    storageBucket: "sushimi-app.appspot.com",
    messagingSenderId: "974005347652",
    appId: "1:974005347652:web:14c505957b7c2443f0a311",
    measurementId: "G-FP26QV05EP"
    };

    const firebaseApp = firebase.initializeApp(firebaseConfig);
    firebase.firestore()
    export default firebaseApp.firestore();
